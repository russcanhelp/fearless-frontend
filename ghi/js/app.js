
function createCard(name, description, pictureUrl, startAndEnd, location) {
    return `
      <div class="card" style="margin-bottom: 30px; box-shadow: 5px 5px rgb(0, 0, 0, 0.3);">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted"> ${location} <p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${startAndEnd}
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.log("bad response")
        alert('Bad Response')
      } else {

        const data = await response.json();
        const columns = document.querySelectorAll('.col')
        let columnIndex = 0;


            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name
                    const startDate = details.conference.starts
                    const endDate = details.conference.ends
                    const startAndEnd = (
                        startDate[5]+ startDate[6] + "/" +
                        startDate[8] + startDate[9] + "/" +
                        startDate[0] + startDate[1] + startDate[2] + startDate[3] )
                         + " - " + (
                        endDate[5] + endDate[6] + "/" +
                        endDate[8] + startDate[9] + "/" +
                        endDate[0] + endDate[1] + endDate[2] + endDate[3])
                    const html = createCard(title, description, pictureUrl, startAndEnd, location);
                    columns[columnIndex].innerHTML += html;

                    // columnIndex = (columnIndex + 1) % columns.length
                    if (columnIndex === 2) {
                        columnIndex = 0
                    } else {
                    columnIndex += 1
                    }
                }
            }
      }
    } catch (e) {
        console.error(e)
      // Figure out what to do if an error is raised
      alert('An error occurred. Please try again later')

    }

  });