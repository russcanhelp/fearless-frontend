import React from 'react';


function Nav() {
  return (
    <React.Fragment>
      <nav className="nav">
        <a className="navbar-brand text-dark" href="/">Conference GO!</a>
        <a className="nav-link text-dark" aria-current="page" href="/">Home</a>
        <a className="nav-link text-secondary" aria-current="page" href="new-location.html">New location</a>
        <a className="nav-link text-secondary" aria-current="page" href="new-conference.html">New conference</a>
        <a className="nav-link text-secondary" aria-current="page" href="new-presentation.html">New presentation</a>
      </nav>
    </React.Fragment>
  );
}

export default Nav;