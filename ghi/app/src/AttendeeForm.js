import React, { useState, useEffect } from 'react'

function AttendeeForm(props) {
    const [name, setName] = useState('')
    const [conferences, setConferences] = useState([])
    const [conference, setConference] = useState('')
    const [email, setEmail] = useState('')


const handleNameChange = (event) => {
    const value = event.target.value
    setName(value)
}

const handleConference = (event) => {
    const value = event.target.value
    setConference(value)
}

const handleEmail = (event) => {
    const value = event.target.value
    setEmail(value)
}

const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        setConferences(data.conferences)
    }
}

const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}
    data.name = name
    data.email = email
    data.conference = conference
    const conferenceId = conference.id

    const attendeeUrl = `http://localhost:8001/api/attendees/`
    const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const response = await fetch(attendeeUrl, fetchConfig)
    if (response.ok) {
        const newAttendee = await response.json()
        setName('')
        setEmail('')
        setConference('')

        alert(`You are now enrolled to attend`)

    }

}

useEffect(() => {
    fetchData()
}, [])

return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Attend a conference</h1>
            <form onSubmit={handleSubmit} id="create-attendee-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEmail} value={email} placeholder="Email" required type="text" name="email" id="email" className="form-control"/>
                <label htmlFor="email">Email</label>
            </div>
            <div className="mb-3">
            <select onChange={handleConference} value={conference} required name="conference" id="conference" className="form-select">
                <option>Choose a conference</option>
                {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    );

}
export default AttendeeForm