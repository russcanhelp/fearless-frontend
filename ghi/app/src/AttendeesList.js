import Nav from './Nav'

function AttendeesList(props) {
    console.log(props)
    if (!props.attendees || props.attendees.length === 0) {
      return <div>No attendees found.</div>;
    }

    return (
          <table className="table table-dark table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Conference</th>
              </tr>
            </thead>
            <tbody>
              {props.attendees.map((attendee) => (
                <tr key={attendee.href}>
                  <td>{attendee.name}</td>
                  <td>{attendee.conference}</td>
                </tr>
              ))}
            </tbody>
          </table>
    );

}


export default AttendeesList